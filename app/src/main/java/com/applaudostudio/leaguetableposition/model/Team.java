package com.applaudostudio.leaguetableposition.model;

import com.applaudostudio.leaguetableposition.R;

import java.util.ArrayList;
import java.util.List;

public class Team {

    private String mName;
    private int mImg;
    private int mGoalF;
    private int mGoalE;
    private int mPoint;

    public Team(String mName, int mImg, int mGoalF, int mGoalE, int mPoint) {
        this.mName = mName;
        this.mImg = mImg;
        this.mGoalF = mGoalF;
        this.mGoalE = mGoalE;
        this.mPoint = mPoint;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public int getImg() {
        return mImg;
    }

    public void setImg(int img) {
        mImg = img;
    }

    public int getGoalF() {
        return mGoalF;
    }

    public void setGoalF(int goalF) {
        mGoalF = goalF;
    }

    public int getGoalE() {
        return mGoalE;
    }

    public void setGoalE(int goalE) {
        mGoalE = goalE;
    }

    public int getPoint() {
        return mPoint;
    }

    public void setPoint(int point) {
        mPoint = point;
    }

    public static List<Team> getTeams(){
        List<Team> teams = new ArrayList<>();
        teams.add(new Team("Bayer Munich",R.drawable.bayern_munich,8,2,15));
        teams.add(new Team("Borussia Dortmund",R.drawable.borussia_dortmund,7,3,12));
        teams.add(new Team("Bayer Leverkusen",R.drawable.bayer_leverkusen,6,4,10));
        teams.add(new Team("FC Shalke 04",R.drawable.fc_schalke_04,5,3,9));
        teams.add(new Team("Hanover 96",R.drawable.hannover_96,4,6,8));
        teams.add(new Team("Borussia Mönchengladbach",R.drawable.borussia_mybet,3,8,7));
        return teams;
    }

}
