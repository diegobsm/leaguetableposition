package com.applaudostudio.leaguetableposition.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.applaudostudio.leaguetableposition.R;
import com.applaudostudio.leaguetableposition.model.Team;

import java.util.ArrayList;
import java.util.List;

public class TableLeagueAdapter extends RecyclerView.Adapter<TableLeagueAdapter.TableLeagueViewHolder> {


    private List<Team> mData;

    public TableLeagueAdapter(){
        mData = new ArrayList<>();
    }

    public void setData(List<Team> data){
        mData = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public TableLeagueViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.team_item,viewGroup,false);
        return new TableLeagueViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TableLeagueViewHolder tableLeagueViewHolder, int i) {
        tableLeagueViewHolder.bind(mData.get(i),(i+1));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    public class TableLeagueViewHolder extends RecyclerView.ViewHolder {

        View view;
        TextView txvPosition;
        TextView txvName;
        ImageView imvLogo;
        TextView txvGoalF;
        TextView txvGoalE;
        TextView txvPoints;

        public TableLeagueViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            txvPosition = itemView.findViewById(R.id.txvPosition);
            imvLogo = itemView.findViewById(R.id.imvLogoTeam);
            txvName = itemView.findViewById(R.id.txvName);
            txvGoalF = itemView.findViewById(R.id.txvGoalF);
            txvGoalE = itemView.findViewById(R.id.txvGoalE);
            txvPoints = itemView.findViewById(R.id.txvPoints);

        }

        public void bind (Team team,int position) {
            txvPosition.setText(String.valueOf(position));
            imvLogo.setImageDrawable(view.getContext().getResources().getDrawable(team.getImg()));
            txvName.setText(team.getName());
            txvGoalF.setText(String.valueOf(team.getGoalF()));
            txvGoalE.setText(String.valueOf(team.getGoalE()));
            txvPoints.setText(String.valueOf(team.getPoint()));
        }
    }
}
