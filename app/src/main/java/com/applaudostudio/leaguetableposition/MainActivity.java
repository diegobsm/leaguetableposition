package com.applaudostudio.leaguetableposition;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.applaudostudio.leaguetableposition.adapter.TableLeagueAdapter;
import com.applaudostudio.leaguetableposition.model.Team;

import java.util.Comparator;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView rcvTableLeague = findViewById(R.id.rcvTableLeague);
        LinearLayoutManager linearLayoutManager =  new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        rcvTableLeague.setLayoutManager(linearLayoutManager);
        TableLeagueAdapter tableLeagueAdapter = new TableLeagueAdapter();
        rcvTableLeague.setAdapter(tableLeagueAdapter);
        List<Team> teams = Team.getTeams();
        tableLeagueAdapter.setData(teams);
    }
}
